# (Short) Snake
What is it? Just a really short snake game. Not the shortest, but surely short!
#### The concept
The goal is to obtain the shortest number of line of beautified js code, so that:
``` js
let a, b, c;
```
will be 3 beautified lines:
```js
let a,
    b,
    c;
```
(Tested with jsBeautify and Prettier on Atom, and jsFiddle beautifier)
#### Live:
https://jsfiddle.net/08fnkzba/7/
#### Disclaimer
Highly inspired by https://jsfiddle.net/Uk2PP/9/
###### And pls don't spell it as SS
