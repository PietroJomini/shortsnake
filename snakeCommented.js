((width, height, length, current, dx, dy, x, y, newEl) => {
  // input handling
  document.body.addEventListener('keydown', e => [dx, dy] = [(e.keyCode - 38) % 2, (e.keyCode - 39) % 2]);
  let timer = setInterval(() => {
    // updating virtual position of head
    [x, y] = [(x + dx) < 0 ? width - 1 : (x + dx) % width, (y + dy) < 0 ? height - 1 : (y + dy) % height];
    // get element at virtual position
    newEl = document.getElementsByClassName(y + '_' + x)[0];
    // food eating and self eating
    if (newEl.className.indexOf('s') > 0) clearInterval(timer), alert('Game Over! Score: ' + length);
    if (newEl.className.indexOf('f') > 0) newEl.className = newEl.className.replace(' f', ''), length += 1;
    // updating real position to virtual position
    newEl.className += ' s', newEl.setAttribute('data-n', current += 1);
    // cleaning last element of the tail
    let item = [...document.getElementsByClassName('s')].sort((e1, e2) => e1.getAttribute('data-n') - e2.getAttribute('data-n'))[0];
    if (document.getElementsByClassName('s').length > length) item.className = item.className.replace(' s', '');
    // spawning new food if necessary
    if (document.getElementsByClassName('f').length == 0) [...document.getElementsByClassName('g')].filter(el => el.className.indexOf('s') < 0).sort(() => 0.5 - Math.random())[0].className += ' f';
  }, 500);
})(10, 10, 5, 1, 1, 0, 0, 0, null);
